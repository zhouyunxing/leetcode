package code.producerandconsumer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumerExample {
    public static void main(String[] args) {
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(5); // 创建大小为5的缓冲区
        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);
        new Thread(producer, "Producer").start();
        new Thread(consumer, "Consumer").start();
    }
}
