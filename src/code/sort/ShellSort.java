package code.sort;

import java.util.Arrays;

public class ShellSort {
    /**
     * 希尔排序
     *
     * @param a
     * @param n
     */
    public static void sort(int[] a, int n){
        if (n <= 1) return;

        int step = n / 2;
        while (step >= 1){
            for (int i = 0; i < step ; i++) {
                for (int j = i; j < n - step ; j += step) {
                    if (a[j+step] < a[j] ){
                        //交换
                        int temp = a[j];
                        a[j] = a[j+step];
                        a[j+step] = temp;
                    }
                }
            }
            step = step / 2;
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{3, 4, 2, 1, 5, 6, 7, 8};
        sort(array, array.length);
        System.out.println(Arrays.toString(array));
    }
}
