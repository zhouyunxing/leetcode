package code.sort;

import java.util.Arrays;

/**
 * 插入排序
 */
public class InsertionSort {

    // 插入排序，a表示数组，n表示数组大小
    public static void sort(int[] a, int n){
        if (n <= 1) return;
        for (int i = 1; i < n ; i++) {
            for (int j = 0; j < i ; j++) {
                if (a[j] > a[i]){
                    int temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{3, 4, 2, 1, 5, 6, 7, 8};
        sort(array, array.length);
        System.out.println(Arrays.toString(array));
    }
}
