package code.sort;

import java.util.Arrays;

/**
 * @ClassName CountSort
 * @Description 计数排序
 * https://blog.csdn.net/afei__/article/details/82959924
 * @Author za-zhouyunxing
 * @Date 2019/10/31 15:06
 * @Version 1.0
 */
public class CountSort {
    public static void main(String[] args) {
        //输入元素均在[0,10)区间内
        int[] arr = new int[]{5, 4, 6, 7, 5, 1, 0, 9, 8, 1};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    private static void sort(int[] arr) {
        int[] countArr = new int[10];
        for (int data : arr) {
            countArr[data]++;
        }
        int index = 0;
        for (int i = 0; i < countArr.length ; i++) {
            while (countArr[i] > 0){
                arr[index++] = i;
                countArr[i]--;
            }
        }

    }
}
