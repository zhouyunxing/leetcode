package code.sort;

import java.util.Arrays;

public class SelectionSort {

    public static void sort(int[] a, int n){
        if (n <= 1)return;
        for (int i = 0; i < n; i++) {
            int minIndex = i;
            for (int j = i; j < n -1; j++) {
                if (a[j+1] < a[j]){
                    minIndex = j+1;
                }
            }
            int temp = a[i];
            a[i] = a[minIndex];
            a[minIndex] = temp;
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{3, 4, 2, 1, 5, 6, 7, 8};
        sort(array, array.length);
        System.out.println(Arrays.toString(array));
    }
}
