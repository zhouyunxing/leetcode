package code.queue;

public class DynamicArrayQueue {
    // 数组：items，数组大小：n
    private String[] items;
    private int n = 0;
    // head表示队头下标，tail表示队尾下标
    private int head = 0;
    private int tail = 0;

    public DynamicArrayQueue(int capacity) {
        this.items = new String[capacity];
        this.n = n;
    }

    // 入队操作，将item放入队尾
    public boolean enqueue(String item) {
        if (tail == n){
            if (head == 0){
                //队列满
                return false;
            }
            // 数据搬移
            for (int i = head; i < tail; ++i) {
                items[i-head] = items[i];
            }
            // 搬移完之后重新更新head和tail
            tail -= head;
            head = 0;
        }
        items[tail] = item;
        tail++;
        return true;
    }

    //出队
    public String dequeue(){
        if (head == tail){
            return null;
        }
        String item = items[head];
        head++;
        return item;
    }

    public void printAll() {
        for (int i = head; i < tail; ++i) {
            System.out.print(items[i] + " ");
        }
        System.out.println();
    }
}
