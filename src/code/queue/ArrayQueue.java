package code.queue;

public class ArrayQueue {
    private String[] items;
    private  int capacity = 0;
    private int head = 0;
    private int tail = 0;

    public ArrayQueue(int capacity) {
        this.items = new String[capacity];
        this.capacity = capacity;
    }

    /**
     * 入队
     * @param item
     */
    public boolean enqueue(String item){
        if (tail == capacity){
            return false;
        }
        items[tail] = item;
        tail++;
        return true;
    }

    /**
     * 出队
     * @return
     */
    public String dequeue(){
        if (head == tail){
            return null;
        }
        String item = items[head];
        items[head] = null;
        head++;
        return item;
    }

    public void printAll() {
        for (int i = head; i < tail; ++i) {
            System.out.print(items[i] + " ");
        }
        System.out.println();
    }
}
