package code.queue;

//循环队列
public class CircularQueue {
    private String items[];
    private int capacity = 0;
    private int head = 0;
    private int tail = 0;

    public CircularQueue(int capacity) {
        this.items = new String[capacity];
        this.capacity = capacity;
    }

    //入队
    public boolean enqueue(String item){
        if ((tail + 1) % capacity == head){
            //队列已满
            return false;
        }
        items[tail] = item;
        tail = (tail + 1) % capacity ;
        return true;
    }

    //出队
    public String dequeue(){
        if (head == tail){
            return  null;
        }
        String item = items[head];
        head = (head + 1) % capacity;
        return item;
    }

    private void printAll(){
        if (capacity == 0){
            return;
        }
        for (int i = head; (i % capacity) != tail; i++){
            System.out.println(items);
        }
    }
}
