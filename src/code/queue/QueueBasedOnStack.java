package code.queue;


import java.util.Stack;

// 用栈实现队列
public class QueueBasedOnStack {
    private Stack<Integer> inStack;
    private Stack<Integer> outStack;

    public QueueBasedOnStack() {
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    public void put(int item) {
        inStack.push(item);
    }

    public int poll() {
        if (outStack.isEmpty()) {
            while (!inStack.isEmpty()) {
                outStack.push(inStack.pop());
            }
        }
        return outStack.pop();
    }

    public boolean isEmpty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    public static void main(String[] args) {
        QueueBasedOnStack queue = new QueueBasedOnStack();
        queue.put(1);
        queue.put(2);
        queue.put(3);
        System.out.println(queue.poll()); // 1
        queue.put(4);
        System.out.println(queue.poll()); // 2
        System.out.println(queue.poll()); // 3
    }
}
