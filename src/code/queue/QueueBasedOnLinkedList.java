package code.queue;

public class QueueBasedOnLinkedList {
    private Node head = null;
    private Node tail = null;

    //入队
    public void enqueue(String value){
        Node node = new Node(value, null);
        if (tail == null){
            //队列空
            head = node;
            tail = node;
        }else {
            tail.next = node;
            tail = tail.next;
        }
    }

    /**
     * 出队
     *
     * @return
     */
    public String dequeue(){
        if (head == null){
            //空队列
            return null;
        }
        String data = head.data;
        head = head.next;
        return data;
    }

    public void printAll() {
        Node p = head;
        while (p != null) {
            System.out.print(p.data + " ");
            p = p.next;
        }
        System.out.println();
    }

    private static class Node {
        private String data;
        private Node next;

        public Node(String data, Node next) {
            this.data = data;
            this.next = next;
        }

        public String getData() {
            return data;
        }
    }
}
