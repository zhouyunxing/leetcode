package code.roundrobin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WeightedRoundRobin {
    private final List<Server> servers;
    private int currentIndex;
    private int maxWeight;
    private int gcdWeight;
    public WeightedRoundRobin(List<Server> servers) {
        this.servers = new ArrayList<>(servers);
        currentIndex = -1;
        if (!servers.isEmpty()) {
            this.maxWeight = findMaxWeight();
            this.gcdWeight = calculateGcd();
        }
    }
    /**
     * 按照加权轮询逻辑检索下⼀个服务器。
     */
    public Server getNextServer(){
        if (servers.isEmpty()) {
            // 没有服务器可⽤
            return null;
        }

        while (true) {
            currentIndex = (currentIndex + 1) % servers.size();
            if (currentIndex == 0) {
                maxWeight -= gcdWeight;
                if (maxWeight <= 0) {
                    maxWeight = findMaxWeight();
                    if (maxWeight == 0) {
                        // 所有服务器的权重都为0，⽆法分配请求
                        return null;
                    }
                }
            }

            if (servers.get(currentIndex).getWeight() >= maxWeight) {
                return servers.get(currentIndex);
            }
        }
    }

    /**
     * 查找所有服务器中的最⼤权重。
     */
    private int findMaxWeight() {
        return servers.stream().mapToInt(Server::getWeight).max().orElse(0);
    }

    /**
     * 计算所有服务器权重的最⼤公约数。
     */
    private int calculateGcd() {
        return servers.stream().mapToInt(Server::getWeight).reduce(this::gcd).orElse(0) ;
    }

    /**
     * 计算两个数的最⼤公约数。
     */
    private int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    public static void main(String[] args) {
        List<Server> servers = Arrays.asList(new Server("ServerA", 1),
                new Server("ServerB", 2),
                new Server("ServerC", 3));
        WeightedRoundRobin wrr = new WeightedRoundRobin(servers);

        // 记录选中次数
        int count1 = 0, count2 = 0, count3 =0;
        int totalCalls = 6000; // 大量次数以验证权重比例
        for (int i=0; i<totalCalls; i++) {
            Server server = wrr.getNextServer();
            switch (server.getName()) {
                case "ServerA":
                    count1++;
                    break;
                case "ServerB":
                    count2++;
                    break;
                case "ServerC":
                    count3++;
                    break;
            }
        }

        System.out.println("ServerA: " + count1); // ServerA: 1000
        System.out.println("ServerB: " + count2); // ServerB: 2000
        System.out.println("ServerC: " + count3); // ServerC: 3000
    }
}
