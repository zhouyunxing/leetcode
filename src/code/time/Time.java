package code.time;

import java.security.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

public class Time {
    public static void main(String[] args) {
        // 1. 使用 java.util.Date 类
        Date now = new Date();
        System.out.println("当前时间：" + now);

        // 2. 使用 java.time 包（Java 8及以上版本推荐使用）
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("当前日期和时间：" + dateTime);
        System.out.println("当前年份:" + dateTime.getYear());
        System.out.println("当前月份:" + dateTime.getMonthValue());
        System.out.println("当前日:" + dateTime.getDayOfMonth());
        System.out.println("当前小时:" + dateTime.getHour());
        System.out.println("当前分钟:" + dateTime.getMinute());
        System.out.println("当前秒:" + dateTime.getSecond());

        LocalDate today = LocalDate.now();
        System.out.println("当前日期：" + today);

        LocalTime time = LocalTime.now();
        System.out.println("当前时间：" + time);

        // 3. 使用 java.util.Calendar 类
        Calendar calendar = Calendar.getInstance();
        System.out.println("当前时间：" + calendar.getTime());

        // 获取时间戳的方法
        // 1. 使用 System.currentTimeMillis()
        long millis = System.currentTimeMillis();
        System.out.println("当前时间戳的毫秒值：" + millis);

        // 2.使用 java.time 包中的 Instant 类（Java 8及以上）
        Instant instant = Instant.now();
        long timestamp = instant.toEpochMilli(); // 转换为毫秒时间戳
        System.out.println("当前时间戳：" + timestamp);

        // 3. 使用 java.sql.Timestamp
        Date now2 = new Date();
        long timestamp2 = now2.getTime();
        System.out.println("当前时间戳：" + timestamp2);
    }
}
