package code.ip2int;

public class Ip2Int {
    public static void main(String[] args) {
        String ip = "192.168.1.1";
        long ipInt = ip2Int(ip);
        System.out.println(ipInt);
    }

    private static long ip2Int(String ip) {
        String[] arr = ip.split("\\.");
        if (arr.length != 4) {
            return -1;
        }
        return (Long.parseLong(arr[0]) << 24) + (Long.parseLong(arr[1]) << 16) + (Long.parseLong(arr[2]) << 8) + Long.parseLong(arr[3]);
    }
}
