package code.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapScan {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(001, "Java");
        map.put(002, "数据库");
        map.put(003, "Vue");
        System.out.println(map);

        // 方式一 通过 Map.keySet 使用 iterator 遍历
        Iterator<Integer> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            Integer key = iterator.next();
            String value = map.get(key);
            System.out.println("key = " + key + ", value = " + value);
        }

        // 方式二 通过 Map.entrySet 使用 iterator 遍历
        Iterator<Map.Entry<Integer, String>> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<Integer, String> entry = entries.next();
            System.out.println(entry);
        }

        // 方式三 通过 Map.keySet 遍历
        for (Integer key : map.keySet()) {
            System.out.println("key = " + key + ", value = " + map.get(key));
        }

        for (String value : map.values()) {
            System.out.println("value = " + value);
        }

        // 方式四 通过 For-Each 迭代 entries，使用 Map.entrySet 遍历
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }

        //方式五 使用 lambda 表达式 forEach 遍历
        map.forEach((k, v) -> System.out.println("key = " + k + ", value = " + v));

    }
}
