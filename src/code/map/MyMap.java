package code.map;

/**
 * @program: HandWriteSourceCode 手写hashmap
 * @description Map接口
 **/
public interface MyMap<K, V> {
    /**
     * 获取
     *
     * @param k
     * @return
     */
    public V get(K k);

    /**
     * 赋值
     *
     * @param k
     * @param v
     * @return
     */
    public V put(K k, V v);

    /**
     * 容量大小
     *
     * @return
     */
    public int size();

    public interface MyEntry<K, V> {
        public K getKey();

        public V getValue();
    }
}

