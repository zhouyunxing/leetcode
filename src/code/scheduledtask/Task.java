package code.scheduledtask;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class Task implements Delayed {
    private String name;

    private long startTime; // milliseconds

    public Task(String _name, long delay) {
        name = _name;
        startTime = System.currentTimeMillis() + delay;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long diff = startTime - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        return (int)(startTime - ((Task)o).startTime);
    }

    public String toString() {
        return "task:"+ name + " at:"+startTime;
    }
}
