package code.scheduledtask;

import java.util.concurrent.DelayQueue;

public class TaskConsumer implements Runnable{
    private DelayQueue<Task> queue;

    public TaskConsumer(DelayQueue<Task> _queue) {
        queue = _queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Task take = queue.take();
                System.out.println("Task "+ take);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
