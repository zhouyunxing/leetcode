package code.scheduledtask;

import java.util.concurrent.DelayQueue;

public class TaskScheduler {
    public static void main(String[] args) {
        DelayQueue<Task> queue = new DelayQueue<>();
        TaskProducer producer = new TaskProducer(queue);
        TaskConsumer consumer = new TaskConsumer(queue);
        new Thread(producer, "Producer Thread").start();
        new Thread(consumer, "Consumer Thread").start();
    }
}
