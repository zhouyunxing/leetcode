package code.concurrent;

public class DeadLockDemo {
    private static final Object resouce1 = new Object(); //资源1
    private static final Object resouce2 = new Object(); //资源2

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (resouce1) {
                System.out.println(Thread.currentThread().getName() + " get resouce1");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " waitting get resouce2");
                synchronized (resouce2) {
                    System.out.println(Thread.currentThread().getName() + " get resouce2");
                }
            }

        }, "ThreadA").start();

        new Thread(()->{
            synchronized (resouce2) {
                System.out.println(Thread.currentThread().getName() + " get resouce2");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " waitting get resouce1");
                synchronized (resouce1) {
                    System.out.println(Thread.currentThread().getName() + " get resouce1");
                }
            }
        }, "ThreadB").start();
    }
}
