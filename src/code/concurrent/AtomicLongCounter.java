package code.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicLongCounter {
    public static void main(String[] args) throws InterruptedException {
        AtomicLong counter = new AtomicLong();

        ExecutorService executor = Executors.newFixedThreadPool(100);
        for (int i=0; i<100; i++) {
            // 100个线程，每个线程累加100次
            executor.submit(()->{
                for (int j=0; j<100; j++) {
                    counter.incrementAndGet();
                }
            });
        }

        executor.shutdown(); // 关闭线程池
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        System.out.println("Value:" + counter.get()); // 输出10000
    }
}
