package code.concurrent.thread;

// 1.继承Thread类并重写run()方法
public class CreatingThread01 extends Thread{
    @Override
    public void run() {
        System.out.println(getName() + " is running");
    }

    public static void main(String[] args) {
        new CreatingThread01().start();
        new CreatingThread01().start();
        new CreatingThread01().start();
        new CreatingThread01().start();
    }
}
