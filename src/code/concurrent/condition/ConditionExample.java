package code.concurrent.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionExample {
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    public void waitForCondition() throws InterruptedException {
        lock.lock();
        try {
            // 等待条件满足
            while (!conditionSatisfied()) {
                condition.await();
            }
            // 临界区代码
        } finally {
            lock.unlock();
        }
    }

    public void signalCondition() {
        lock.lock();
        try {
            // 改变条件状态
            changeConditionState();
            // 唤醒一个等待的线程
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    private boolean conditionSatisfied() {
        // 条件检查逻辑
        return false;
    }

    private void changeConditionState() {
        // 改变条件状态的逻辑
    }
}
