package code.concurrent.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockExample {
    private final Lock lock = new ReentrantLock();

    public void performAction() {
        // 加锁
        lock.lock();
        try {
            // 临界区代码
        } finally {
            // 释放锁
            lock.unlock();
        }
    }
}
