package code;

// https://www.geeksforgeeks.org/find-the-least-common-multiple-lcm-of-given-strings/
public class lcm {
    // Function to find the lcm of both Strings
    static String findLcm(String str1, String str2)
    {
        String x = str1, y = str2;

        // While their length is unequal
        while (x.length() != y.length()) {
            if (x.length() < y.length())
                x += str1;
            else
                y += str2;
        }

        if (x.equals(y))
            return x;
        return "";
    }

    // Driver Code
    public static void main(String[] args)
    {
        String str1 = "ba", str2 = "baba";

        String ans = findLcm(str1, str2);
        if (ans != "")
            System.out.println(ans);
        else {
            System.out.println(-1);
        }

        // 用例2
        String str3 = "aba", str4 = "ab";

        String ans2 = findLcm(str3, str4);
        if (ans2 != "")
            System.out.println(ans2);
        else {
            System.out.println(-1);
        }
    }
}
