package code.blockingqueue;

public class BlockingQueue {
    private int[] items;
    private volatile int head = 0; // 头指针
    private volatile int tail = 0; // 尾指针
    private volatile int size = 0; // 队列中当前的元素
    public BlockingQueue(int capacity) {
        this.items = new int[capacity];
    }

    // 入队
    public void put(int item) throws InterruptedException {
        synchronized (this) {
            while (size >= items.length) {
                // 队列满了，采取等待策略
                System.out.println("生产者等待");
                this.wait();
            }
            // 队列没满，添加元素
            if (tail >= items.length) {
                tail = 0;
            }
            items[tail] = item;
            tail++;
            size++;
            // 成功入队，唤醒因为队列空间被阻塞进入的状态
            this.notify();
        }
    }

    // 出队
    public int take() throws InterruptedException {
        synchronized (this) {
            while (size == 0) {
                // 队列为空了，采取等待策略
                System.out.println("消费者等待");
                this.wait();
            }
            // 队列不为空
            if (head >= items.length) {
                head = 0;
            }
            int item = items[head];
            head++;
            size--;
            this.notify();//使用这个notify唤醒队列满的阻塞状态
            return  item;
        }
    }

    public static void main(String[] args) {
        BlockingQueue queue = new BlockingQueue(10);
        // 生产者线程
        Thread producer = new Thread(()->{
            int count = 0;
            for (int i=0; i<100; i++){
                try {
                    queue.put(count);
                    System.out.println("生产元素:" + count);
                    count++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
         });

        Thread consumer = new Thread(()->{
            while (true) {
                try {
                    int item = queue.take();
                    System.out.println("消费元素:" + item);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.start();
        consumer.start();
    }
}
