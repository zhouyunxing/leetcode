package code.bit;

import java.util.LinkedList;
import java.util.List;

// https://www.acwing.com/blog/content/25361/
// 每次可以把一个字符变成两个字母表中比它小一位的字符。例如，可以把‘b’变成两个‘a’，可以把‘z’变成两个‘y’。希望最终可以生成x个’a’，求出初始的字符串？
// 5 : "aaaaa" -> "bba" -> "ca"
public class P1 {
    public static void main(String[] args) {
        System.out.println(test(4)); // "aaaa" -> "bb" -> "c"
        System.out.println(test(5));  // "aaaaa" -> "bba" -> "ca"
        System.out.println(test(7));  // "aaaaaaa" -> "bbba" -> "cba"
    }

    public static String test(int x) {
        List<Character> list = new LinkedList<>();
        int m = 0;
        while (x > 0) {
            if ((x & 1) == 1) {
                list.add((char)('a'+m));
            }
            m++;
            x = x>>1;
        }
        return list.toString();
    }
}
