package com.zhouyx;

// 给定m个不重复的字符 [a, b, c, d]，以及一个长度为n的字符串tbcacbdata，
// 问能否在这个字符串中找到一个长度为m的连续子串，使得这个子串刚好由上面m个字符组成，顺序无所谓，
// 返回任意满足条件的一个子串的起始位置，未找到返回-1。比如上面这个例子，返回3。
public class SlidingWindowDemo {
    public int getIndex(byte[] array, String str) {
        int m = array.length;
        int n = str.length();
        if (m > n) {
            return -1;
        }
        // 滑动窗口
        for (int i=0; i<= n-m; i++) {
            String temp = str.substring(i, i+m);
            if (checkSubStr(array, temp)) {
                return  i;
            }
        }
        return -1;
    }

    private boolean checkSubStr(byte[] array, String str) {
        for (byte ch : array) {
            if (str.indexOf(ch) == -1) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        SlidingWindowDemo demo = new SlidingWindowDemo();
        System.out.println(demo.getIndex(new byte[]{'a', 'b', 'c', 'd'}, "tbcacbdata"));
    }
}
