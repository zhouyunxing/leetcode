package com.zhouyx;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @program: leetcode
 * @description
 * @author: za-zhouyunxing
 * @create: 2020-05-29 23:31
 **/
public class Test {
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.offer(2);
        queue.offer(3);
        queue.offer(1);
        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }
}
