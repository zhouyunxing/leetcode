package com.zhouyx;

import java.util.Scanner;

/**
 * @program: leetcode
 * @description
 * @author: za-zhouyunxing
 * @create: 2020-05-29 16:20
 **/
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //第一行包括一个数字N，表示本次用例包括多少个待校验的字符串
        int n = Integer.parseInt(sc.nextLine());

        while(sc.hasNext()) {

            for(int i=0;i<n;i++){
                StringBuilder stringBuilder = new StringBuilder(sc.nextLine());
                for(int j=2;j<stringBuilder.length();j++){

                    //三个同样的字母连在一起，一定是拼写错误，去掉一个的就好啦：比如 helllo -> hello
                    if(stringBuilder.charAt(j) == stringBuilder.charAt(j-1) && stringBuilder.charAt(j-1) == stringBuilder.charAt(j-2)){
                        stringBuilder.deleteCharAt(j);
                        j--;
                    }
                    if(j-3<0){
                        continue;
                    }
//上面的规则优先“从左到右”匹配，即如果是AABBCC，虽然AABB和BBCC都是错误拼写，应该优先考虑修复AABB，结果为AABCC
                    if(stringBuilder.charAt(j-3) == stringBuilder.charAt(j-2) && stringBuilder.charAt(j-1) == stringBuilder.charAt(j)) {
                        stringBuilder.deleteCharAt(j);
                        j--;
                    }

                }
                System.out.println(stringBuilder.toString());

            }
            //sc.close();这个需要删除，如果不删除提交答案就会出错
        }
    }
}
