package leetcode.d_200_299;

import java.util.List;

/**
 * @program: leetcode
 * @description 多叉数
 * @author: za-zhouyunxing
 * @create: 2020-05-31 16:47
 **/
public class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
}
