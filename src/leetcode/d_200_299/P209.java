package leetcode.d_200_299;

public class P209 {
    public static void main(String[] args) {
        P209.Solution solution = new P209().new Solution();
        System.out.println(solution.minSubArrayLen(7, new int[]{2,3,1,2,4,3}));
    }

    // https://leetcode.cn/problems/minimum-size-subarray-sum/description/?envType=study-plan-v2&envId=top-interview-150
    // 长度最小的子数组
    class Solution {
        public int minSubArrayLen(int target, int[] nums) {
            int minLen = Integer.MAX_VALUE;
            int left = 0;
            int right=0;
            int sum = 0;
            while (right < nums.length) {
                sum += nums[right];
                while(sum >= target) {
                    minLen = Math.min(minLen, right-left+1);
                    sum -= nums[left];
                    left++;
                }
                right++;
            }
            if (minLen == Integer.MAX_VALUE) {
                return 0;
            }
            return minLen;
        }
    }
}
