package leetcode.d_200_299;

import java.util.HashMap;
import java.util.Map;

public class P205 {

    public boolean isIsomorphic(String s, String t) {
        int m = s.length();
        int n = t.length();
        if (m != n) {
            return false;
        }
        Map<Character, Character> s2t = new HashMap<>();
        Map<Character, Character> t2s = new HashMap<>();
        for (int i=0; i<m; i++) {
            char ch1 = s.charAt(i);
            char ch2 = t.charAt(i);
            if (s2t.containsKey(ch1) && s2t.get(ch1) != ch2 ) {
                return false;
            }
            if (t2s.containsKey(ch2) && t2s.get(ch2) != ch1) {
                return false;
            }
            s2t.put(ch1, ch2);
            t2s.put(ch2, ch1);
        }
        return true;
    }
}
