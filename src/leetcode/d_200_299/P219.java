package leetcode.d_200_299;

import java.util.HashMap;
import java.util.Map;

public class P219 {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i=0; i<nums.length; i++) {
            if (map.containsKey(nums[i])) {
                int preIdx = map.get(nums[i]);
                if (i-preIdx <= k) {
                    return true;
                }
            }
            map.put(nums[i], i);
        }
        return false;
    }
}
