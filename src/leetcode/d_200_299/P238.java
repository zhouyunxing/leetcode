package leetcode.d_200_299;

public class P238 {
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] left = new int[n];
        left[0] = 1;
        for (int i=1; i<n; i++) {
            left[i] = nums[i-1]*left[i-1];
        }
        int R = 1;
        int[] res = new int[n];
        for (int i=n-1; i>=0; i--) {
            res[i] = left[i] * R;
            R = nums[i]*R;
        }
        return res;
    }
}
