package leetcode.d_200_299;

/**
 * @program: leetcode
 * @description
 * @author: za-zhouyunxing
 * @create: 2020-04-11 17:16
 **/
public class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}
