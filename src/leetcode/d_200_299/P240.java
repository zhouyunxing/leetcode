package leetcode.d_200_299;

public class P240 {
    class Solution {
        public boolean searchMatrix(int[][] matrix, int target) {
            int m = matrix.length;
            int n = matrix[0].length;
            int i = 0;
            int j = n-1;
            while(i<m && j>=0) {
                if (matrix[i][j] == target) {
                    return true;
                }
                if (matrix[i][j] > target) {
                    j--;
                }else {
                    i++;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}};
        P240.Solution solution = new P240().new Solution();
        System.out.println(solution.searchMatrix(matrix, 5));
    }
}
