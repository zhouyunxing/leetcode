package leetcode.d_200_299;

import java.util.HashMap;
import java.util.Map;

public class P290 {
    public boolean wordPattern(String pattern, String s) {
        char[] chars = pattern.toCharArray();
        int m = chars.length;
        String[] sArr = s.split(" ");
        int n = sArr.length;
        if (m != n) {
            return false;
        }
        Map<Character, String> p2s = new HashMap();
        Map<String, Character> s2p = new HashMap();
        for (int i=0; i<m; i++) {
            char ch = chars[i];
            String ss = sArr[i];
            if (p2s.containsKey(ch) && !p2s.get(ch).equals(ss)) {
                return false;
            }
            if (s2p.containsKey(ss) && s2p.get(ss) != ch) {
                return false;
            }
            p2s.put(ch, ss);
            s2p.put(ss, ch);
        }
        return true;
    }
}
