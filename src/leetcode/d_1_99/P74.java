package leetcode.d_1_99;

public class P74 {
    class Solution {
        public boolean searchMatrix(int[][] matrix, int target) {
            int m = matrix.length;
            int n = matrix[0].length;
            // 从左下角开始扫描
            int i = m-1;
            int j = 0;
            while (i>=0 && j<n) {
                if (matrix[i][j] == target) {
                    return true;
                }
                if (matrix[i][j] > target) {
                    i--;
                }else{
                    j++;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{1,3,5,7}, {10,11,16,20}, {23,30,34,60}};
        P74.Solution solution = new P74().new Solution();
        System.out.println(solution.searchMatrix(matrix, 3));
    }
}
