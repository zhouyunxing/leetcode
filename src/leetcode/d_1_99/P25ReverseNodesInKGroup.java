//给你一个链表，每 k 个节点一组进行翻转，请你返回翻转后的链表。 
//
// k 是一个正整数，它的值小于或等于链表的长度。 
//
// 如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。 
//
// 
//
// 示例： 
//
// 给你这个链表：1->2->3->4->5 
//
// 当 k = 2 时，应当返回: 2->1->4->3->5 
//
// 当 k = 3 时，应当返回: 3->2->1->4->5 
//
// 
//
// 说明： 
//
// 
// 你的算法只能使用常数的额外空间。 
// 你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。 
// 
// Related Topics 链表


package leetcode.d_1_99;

//Java：K 个一组翻转链表
public class P25ReverseNodesInKGroup{
    public static void main(String[] args) {
        Solution solution = new P25ReverseNodesInKGroup().new Solution();
        // TO TEST
    }
    //leetcode submit region begin(Prohibit modification and deletion)
    /**
     * Definition for singly-linked list.
     * public class ListNode {
     *     int val;
     *     ListNode next;
     *     ListNode(int x) { val = x; }
     * }
     */
    class Solution {
        public ListNode reverseKGroup(ListNode head, int k) {
            if (head == null || head.next == null){
                return head;
            }
            ListNode tail = head;
            for (int i = 0; i < k; i++) {
                //剩余数量小于K的话，返回head
                if (tail == null){
                    return head;
                }
                tail = tail.next;
            }
            //反转前k个元素
            ListNode newHead = reverse(head, tail);
            //下一轮开始的地方就是tail
            head.next = reverseKGroup(tail, k);
            return newHead;
        }

        /**
         * 反转前k个元素
         * 前闭后开
         *
         * @param head
         * @param tail
         * @return
         */
        private ListNode reverse(ListNode head, ListNode tail){
            ListNode pre = null;
            while (head != tail){
                ListNode nextTemp = head.next;
                head.next = pre;
                pre = head;
                head = nextTemp;
            }
            return pre;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}