package leetcode.d_1_99;

// https://leetcode.cn/problems/search-insert-position/description/
// 搜索插入位置
public class P35 {
    class Solution {
        public int searchInsert(int[] nums, int target) {
            if (nums.length == 0) {
                return -1;
            }
            int left = 0;
            int right = nums.length - 1;
            while(left <= right) {
                int mid = left + (right - left) / 2;
                if (nums[mid] == target) {
                    return mid;
                }
                if (nums[mid] > target) {
                    right = mid - 1;
                }
                if (nums[mid] < target) {
                    left = mid + 1;
                }
            }
            return left;
        }
    }

    public static void main(String[] args) {
        P35.Solution solution = new P35().new Solution();
        int[] nums = {1,3,5,6};
        System.out.println(solution.searchInsert(nums, 5));
        System.out.println(solution.searchInsert(nums, 2));
    }
}
