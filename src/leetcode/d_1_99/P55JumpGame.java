package leetcode.d_1_99;

public class P55JumpGame {
    public static void main(String[] args) {
        P55JumpGame.Solution solution = new P55JumpGame().new Solution();
        int[] nums = {2,3,1,1,4};
        boolean res = solution.CanJump(nums);
        System.out.println(res);

        int[] nums2 = {3,2,1,0,4};
        boolean res2 = solution.CanJump(nums2);
        System.out.println(res2);
    }

    class Solution {
        public boolean CanJump(int[] nums) {
            int maxLen = 0;
            for (int i =0; i < nums.length; i++) {
                if (i > maxLen) {
                    return false;
                }
                maxLen = Math.max(maxLen, i + nums[i]);
            }
            return  true;
        }
    }
}
