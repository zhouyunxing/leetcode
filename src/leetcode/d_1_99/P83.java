package leetcode.d_1_99;

public class P83 {
      public class ListNode {
          int val;
          ListNode next;
          ListNode() {}
          ListNode(int val) { this.val = val; }
          ListNode(int val, ListNode next) { this.val = val; this.next = next; }
      }

    class Solution {
        public ListNode deleteDuplicates(ListNode head) {
            if (head == null) {
                return null;
            }
            ListNode cur = head;
            while (cur.next != null) {
                if (cur.next.val == cur.val) {
                    cur.next = cur.next.next;
                }else {
                    cur = cur.next;
                }
            }
            return head;
        }
    }

    public static void main(String[] args) {
        P83.ListNode head = new P83().new ListNode(1, new P83().new ListNode(1, new P83().new ListNode(2, new P83().new ListNode(3, new P83().new ListNode(3, null)))));
        P83.Solution solution = new P83().new Solution();
        ListNode res = solution.deleteDuplicates(head);
        while (res != null) {
            System.out.print(res.val);
            res = res.next;
        }
    }
}
