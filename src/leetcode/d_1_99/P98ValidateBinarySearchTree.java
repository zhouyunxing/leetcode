//给定一个二叉树，判断其是否是一个有效的二叉搜索树。 
//
// 假设一个二叉搜索树具有如下特征： 
//
// 
// 节点的左子树只包含小于当前节点的数。 
// 节点的右子树只包含大于当前节点的数。 
// 所有左子树和右子树自身必须也是二叉搜索树。 
// 
//
// 示例 1: 
//
// 输入:
//    2
//   / \
//  1   3
//输出: true
// 
//
// 示例 2: 
//
// 输入:
//    5
//   / \
//  1   4
//     / \
//    3   6
//输出: false
//解释: 输入为: [5,1,4,null,null,3,6]。
//     根节点的值为 5 ，但是其右子节点值为 4 。
// 
// Related Topics 树 深度优先搜索


package leetcode.d_1_99;

//Java：验证二叉搜索树
public class P98ValidateBinarySearchTree{
    public static void main(String[] args) {
        Solution solution = new P98ValidateBinarySearchTree().new Solution();
        // TO TEST
    }
    //leetcode submit region begin(Prohibit modification and deletion)
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    class Solution {
        long pre = Long.MIN_VALUE;
        public boolean isValidBST(TreeNode root) {
            if (root == null){
                return true;
            }
            //访问左子树
            if (!isValidBST(root.left)){
                return false;
            }

            // 访问当前节点：如果当前节点小于等于中序遍历的前一个节点，说明不满足BST，返回 false；否则继续遍历。
            if (root.val <= pre){
                return false;
            }
            pre = root.val;

            //访问右子树
            if (!isValidBST(root.right)){
                return false;
            }

            return true;
        }


        // 第2种方法
        public boolean isValidBST2(TreeNode root) {
            long max = Long.MAX_VALUE;
            long min = Long.MIN_VALUE;
            return helper(root, max, min);
        }

        public boolean helper(TreeNode root, long max, long min) {
            if (root == null) {
                return true;
            }
            if (!(root.val < max && root.val > min)) {
                return false;
            }
            return helper(root.left, root.val, min) && helper(root.right, max, root.val);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}