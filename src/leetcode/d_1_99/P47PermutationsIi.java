//给定一个可包含重复数字的序列，返回所有不重复的全排列。 
//
// 示例: 
//
// 输入: [1,1,2]
//输出:
//[
//  [1,1,2],
//  [1,2,1],
//  [2,1,1]
//] 
// Related Topics 回溯算法


package leetcode.d_1_99;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//Java：全排列 II
public class P47PermutationsIi{
    public static void main(String[] args) {
        Solution solution = new P47PermutationsIi().new Solution();
        // TO TEST
    }
    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        List<List<Integer>> res = new LinkedList<>();
        boolean[] userd;
        int len;
        int[] nums;

        public List<List<Integer>> permuteUnique(int[] nums) {
            if (nums == null || nums.length == 0){
                return res;
            }
            // 排序（升序或者降序都可以），排序是剪枝的前提
            Arrays.sort(nums);
            this.nums = nums;
            len = nums.length;
            userd = new boolean[len];

            dfs(0, new LinkedList<Integer>());
            return res;
        }

        private void dfs(int depth, LinkedList<Integer> path) {
            if (depth == len){
                res.add(new LinkedList<>(path));
            }
            for (int i = 0; i < len; i++) {
                if (userd[i]){
                    continue;
                }

                //剪枝
                if (i > 0 && nums[i] == nums[i-1] && !userd[i-1]){
                    continue;
                }

                path.add(nums[i]);
                userd[i] = true;

                dfs(depth + 1, path);

                path.removeLast();
                userd[i] = false;
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}