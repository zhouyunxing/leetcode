package leetcode.d_1_99;

// https://leetcode.cn/problems/length-of-last-word/
// 最后一个单词的长度
public class P58 {
    public int lengthOfLastWord(String s) {
        int len = s.length();
        int i = len-1;
        int res = 0;
        while(i>=0 && s.charAt(i) == ' ') {
            i--;
        }
        while(i>=0 && s.charAt(i) != ' ') {
            res++;
            i--;
        }
        return res;
    }

    public static void main(String[] args) {
        P58 test = new P58();
        System.out.println(test.lengthOfLastWord("   fly me   to   the moon  ")); // 4
    }
}
