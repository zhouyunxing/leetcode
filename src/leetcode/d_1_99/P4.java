package leetcode.d_1_99;

import java.util.*;

// https://leetcode.cn/problems/median-of-two-sorted-arrays/description/
// 寻找2个正序数组的中位数
public class P4 {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] nums = new int[nums1.length + nums2.length];
        System.arraycopy(nums1, 0, nums, 0, nums1.length);
        System.arraycopy(nums2, 0, nums, nums1.length, nums2.length);
        Arrays.sort(nums);

        if (nums.length % 2 == 0) {
            int mid = nums.length / 2;
            double ans = ((double)nums[mid-1] + nums[mid]) / 2;
            return ans;
        }else {
            int mid = (nums.length-1) / 2;
            return (double)nums[mid];
        }
    }
}
