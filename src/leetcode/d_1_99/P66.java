package leetcode.d_1_99;

import java.util.Arrays;

public class P66 {
    public static void main(String[] args) {
        P66 test = new P66();
        System.out.println(Arrays.toString(test.plusOne(new int[]{1,2,3}))); // 1,2,4
        System.out.println(Arrays.toString(test.plusOne(new int[]{9,9,9}))); // 1,0,0,0
    }

    public int[] plusOne(int[] digits) {
        for(int i=digits.length-1; i>=0; i--) {
            digits[i]++;
            if (digits[i] != 10) {
                return digits;
            }else {
                digits[i] = 0;
            }
        }

        // digits 中所有的元素均为 9
        int[] res = new int[digits.length+1];
        res[0] = 1; // 首位为1， 其余元素都为0
        return res;
    }
}
