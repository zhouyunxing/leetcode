package leetcode.d_1_99;

// https://leetcode.cn/problems/multiply-strings/solutions/29100/you-hua-ban-shu-shi-da-bai-994-by-breezean/
// 字符串相乘
public class P43 {
    public String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }

        int m = num1.length();
        int n = num2.length();
        int[] res = new int[m+n];
        for (int i=m-1; i>=0; i--) {
            int a = num1.charAt(i) - '0';
            for (int j=n-1; j>=0; j--) {
                int b = num2.charAt(j) - '0';
                int sum = res[i+j+1] + a*b;
                res[i+j+1] = sum % 10;
                res[i+j] += sum/10;
            }
        }

        StringBuilder b = new StringBuilder();
        for (int i=0; i<res.length; i++) {
            if (i==0 && res[i] == 0) {
                continue;
            }
            b.append(res[i]);
        }
        return b.toString();
    }
}
