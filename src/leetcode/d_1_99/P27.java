package leetcode.d_1_99;

// https://leetcode.cn/problems/remove-element/description/
// 移除元素
public class P27 {
    public int removeElement(int[] nums, int val) {
        int j=0;
        for(int i=0; i<nums.length; i++) {
            if (nums[i] != val) {
                nums[j++] = nums[i];
            }
        }
        return j;
    }

    public static void main(String[] args) {
        P27 test = new P27();
        System.out.println(test.removeElement(new int[]{3,2,2,3}, 3));
        String s = "abc";
        System.out.println(s.substring(2,3));
    }
}
