package leetcode.d_1_99;

// https://leetcode.cn/problems/rotate-image/description/
// 旋转图像
public class P48 {
    public void rotate(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        // 1. 引对角线交换
        for(int i=0; i<m; i++) {
            for(int j=i; j<n; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }

        // 翻转每一行
        for(int i=0; i<m; i++) {
            int left = 0;
            int right = n-1;
            while(left < right) {
                int temp = matrix[i][left];
                matrix[i][left] = matrix[i][right];
                matrix[i][right] = temp;
                left++;
                right--;
            }
        }
    }
}
