package leetcode.d_300_plus;

public class P628 {
    public static void main(String[] args) {
        P628.Solution solution = new P628().new Solution();
        System.out.println(solution.maximumProduct(new int[]{1,2,3})); // 6
        System.out.println(solution.maximumProduct(new int[]{-1,-2,-3})); // -6
    }

    class Solution {
        public int maximumProduct(int[] nums) {
            int max1 = Integer.MIN_VALUE;
            int max2 = Integer.MIN_VALUE;
            int max3 =  Integer.MIN_VALUE; // 分别是第1大，第2大，第3大

            int min1 = Integer.MAX_VALUE;
            int min2 = Integer.MAX_VALUE; // 分别是第1小，第2小

            for(int x : nums) {
                if (x < min1) {
                    min2 = min1;
                    min1 = x;
                }else if (x < min2) {
                    min2 = x;
                }

                if (x > max1) {
                    max3 = max2;
                    max2 = max1;
                    max1 = x;
                }else if (x > max2) {
                    max3 = max2;
                    max2 = x;
                }else if (x > max3) {
                    max3 = x;
                }
            }

            return Math.max(max1*max2*max3, min1*min2*max1);
        }
    }
}
