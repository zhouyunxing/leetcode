package leetcode.d_300_plus;

public class P461 {
    public int hammingDistance(int x, int y) {
        int n = x ^ y;
        int sum = 0;
        while (n != 0) {
            n = n & (n-1);
            sum++;
        }
        return sum;
    }
}
