package leetcode.d_300_plus;

public class P494 {
    int res;
    public int findTargetSumWays(int[] nums, int target) {
        res = 0;
        dfs(nums, target, 0, 0);
        return res;
    }

    public void dfs(int[] nums, int target,int idx, int sum) {
        if (idx == nums.length) {
            if (sum == target) {
                res++;
            }
            return;
        }
        // +当前元素
        dfs(nums, target, idx+1, sum+nums[idx]);
        // -当前元素
        dfs(nums, target, idx+1, sum-nums[idx]);
    }
}
