package leetcode.d_300_plus;

public class P538 {
    int sum;
    public TreeNode convertBST(TreeNode root) {
        sum = 0;
        helper(root);
        return root;
    }

    public void helper(TreeNode root) {
        if (root == null) {
            return;
        }
        // 右树
        helper(root.right);
        root.val = root.val + sum;
        sum = root.val;
        // 左数
        helper(root.left);
    }
}
