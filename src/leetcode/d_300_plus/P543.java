package leetcode.d_300_plus;

public class P543 {
    int max;
    public int diameterOfBinaryTree(TreeNode root) {
        max = 1;
        maxDepth(root);
        return max-1;
    }

    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int rightDepth = maxDepth(root.right);
        int leftDepth = maxDepth(root.left);
        max = Math.max(max, 1+rightDepth+leftDepth); // 更新结果，这个节点的最大直径
        return 1+Math.max(rightDepth, leftDepth); // 当前节点的最大深度
    }
}
