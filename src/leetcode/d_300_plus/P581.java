package leetcode.d_300_plus;

public class P581 {
    // 左往右看：就是一个升序数组，时刻更新max，如果突然是降序，那么它肯定有问题，记录为右边界。
    // 右往左看：就是一个降序数组，时刻更新min，如果突然是升序，那么它肯定有问题，记录为左边界。
    public int findUnsortedSubarray(int[] nums) {
        int n = nums.length;
        if (n <= 1) {
            return 0;
        }

        int left = 0;
        int right = -1;
        int max = nums[0];
        int min = nums[n-1];
        for (int i=0; i<n; i++) {
            if (nums[i] >= max) {
                max = nums[i];
            }else {
                right = i;
            }
        }
        for (int i=n-1; i>=0; i--) {
            if (nums[i] <= min) {
                min = nums[i];
            }else {
                left = i;
            }
        }
        return right-left+1;
    }
}
