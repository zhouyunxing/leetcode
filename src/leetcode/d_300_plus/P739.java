package leetcode.d_300_plus;

import java.util.Stack;

public class P739 {
    public int[] dailyTemperatures(int[] temperatures) {
        int n = temperatures.length;
        if (n == 0) {
            return null;
        }
        int[] answer = new int[n];
        Stack<Integer> stack = new Stack<>();
        // Deque<Integer> stack = new ArrayDeque<>();
        for (int i=0; i<n; i++) {
            while (!stack.isEmpty() && temperatures[stack.peek()] < temperatures[i]) {
                answer[stack.peek()] = i-stack.peek();
                stack.pop();
            }
            stack.push(i);
        }
        return answer;
    }
}
