package leetcode.d_300_plus;

import java.util.ArrayList;
import java.util.List;

public class P448 {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        int n = nums.length;
        List<Integer> res = new ArrayList<>();
        if (n == 0) {
            return res;
        }
        for (int num: nums) {
            int v = (num-1) % n;
            nums[v] = nums[v] + n;
        }
        for (int i=0; i<n; i++) {
            if (nums[i] <= n) {
                res.add(i+1);
            }
        }
        return res;
    }
}
