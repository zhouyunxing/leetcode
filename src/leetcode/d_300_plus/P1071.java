package leetcode.d_300_plus;

public class P1071 {
    public static void main(String[] args) {
        P1071 test = new P1071();
        System.out.println(test.gcdOfStrings("ABABAB", "AB")); // AB
        System.out.println(test.gcdOfStrings("ABABABAB", "ABAB")); // ABAB
    }

    public String gcdOfStrings(String str1, String str2) {
        if (!str1.concat(str2).equals(str2.concat(str1))) {
            return "";
        }
        return str1.substring(0, gcd(str1.length(), str2.length()));
    }

    public int gcd(int a, int b) {
        while (b != 0) {
            int temp = a;
            a = b;
            b = temp % b;
        }
        return a;
    }
}
