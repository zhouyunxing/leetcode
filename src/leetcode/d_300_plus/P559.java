package leetcode.d_300_plus;

import java.util.*;

public class P559 {
    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    class Solution {
        public int maxDepth(Node root) {
            if (root == null) {
                return 0;
            }
            int maxDepth = 0;
            for (Node node : root.children) {
                maxDepth = Math.max(maxDepth, maxDepth(node));
            }
            return maxDepth + 1;
        }
    }

    public static void main(String[] args) {
        P559.Solution solution = new P559().new Solution();
        P559.Node root = new P559().new Node(1, Arrays.asList(new P559().new Node(2, new ArrayList<>()), new P559().new Node(3, new ArrayList<>()), new P559().new Node(4, new ArrayList<>())));
        System.out.println(solution.maxDepth(root));
    }
}
