package leetcode.d_300_plus;

public class P647 {
    public int countSubstrings(String s) {
        int n = s.length();
        if (n == 0) {
            return 0;
        }
        int res = 0;
        for (int i=0; i<n; i++) {
            for (int j=0; j<=i; j++) {
                String temp = s.substring(j, i+1);
                if (helper(temp)) {
                    res++;
                }
            }
        }
        return res;
    }

    public boolean helper(String s) {
        int n = s.length();
        int left = 0;
        int right = n-1;
        while (left < right) {
            char ch1 = s.charAt(left);
            char ch2 = s.charAt(right);
            if (ch1 != ch2) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
