package leetcode.d_300_plus;

// https://leetcode.cn/problems/is-subsequence/?envType=study-plan-v2&envId=top-interview-150
// 判断子序列
public class P392 {
    public static void main(String[] args) {
        P392.Solution solution = new P392().new Solution();
        System.out.println(solution.isSubsequence("abc", "ahbgdc"));
    }

    class Solution {
        public boolean isSubsequence(String s, String t) {
            int len1 = s.length();
            int len2 = t.length();
            int idx1 = 0; // s的指针
            int idx2 =0; // t的指针
            while (idx1 < len1 && idx2 < len2) { // t的指针
                if (s.charAt(idx1) == t.charAt(idx2)) {
                    idx1++;
                }
                idx2++;
            }
            return idx1 == len1;
        }
    }
}
