package leetcode.d_300_plus;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class P703 {
    class KthLargest {
        Queue<Integer> queue;
        int size;

        public KthLargest(int k, int[] nums) {
            this.size = k;
            this.queue = new PriorityQueue<>((v1, v2)-> v1-v2);
            for (int num : nums) {
                queue.offer(num);
            }
        }

        public int add(int val) {
            this.queue.offer(val);
            while(this.queue.size() > this.size) {
                this.queue.poll();
            }
            return queue.peek();
        }
    }
}
