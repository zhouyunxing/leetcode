package leetcode.d_300_plus;

import java.util.HashMap;

public class P383 {
    public boolean canConstruct(String ransomNote, String magazine) {
        HashMap<Character, Integer> magazineMap = new HashMap<>();
        for (int i=0; i<magazine.length(); i++) {
            char ch = magazine.charAt(i);
            int count = magazineMap.getOrDefault(ch, 0);
            magazineMap.put(ch, count+1);
        }
        for (int i=0; i<ransomNote.length(); i++) {
            char ch = ransomNote.charAt(i);
            if (!magazineMap.containsKey(ch)) {
                return false;
            }
            int count = magazineMap.get(ch);
            if (count == 0) {
                return false;
            }
            magazineMap.put(ch, count-1);
        }
        return true;
    }
}
