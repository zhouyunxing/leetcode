package leetcode.d_300_plus;

import java.util.Arrays;

public class P567 {
    class Solution {
        public boolean checkInclusion(String s1, String s2) {
            int m = s1.length();
            int n = s2.length();
            if (m > n) {
                return false;
            }

            int[] char1 = new int[26];
            int[] char2 = new int[26];
            for (int i=0; i<m; i++) {
                char1[s1.charAt(i)-'a']++;
                char2[s2.charAt(i)-'a']++;
            }
            if (Arrays.equals(char1, char2)) {
                return true;
            }
            // 滑动窗口
            for (int i=m; i<n ; i++) {
                char2[s2.charAt(i)-'a']++;
                char2[s2.charAt(i-m)-'a']--;
                if (Arrays.equals(char1, char2)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) {
        P567.Solution solution = new P567().new Solution();
        System.out.println(solution.checkInclusion("ab", "eidbaooo")); // true
        System.out.println(solution.checkInclusion("ab", "eidboaoo")); // false
    }
}
