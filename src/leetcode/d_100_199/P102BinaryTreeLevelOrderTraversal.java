//给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。 
//
// 
//
// 示例： 
//二叉树：[3,9,20,null,null,15,7], 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7
// 
//
// 返回其层次遍历结果： 
//
// [
//  [3],
//  [9,20],
//  [15,7]
//]
// 
// Related Topics 树 广度优先搜索


package leetcode.d_100_199;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//Java：二叉树的层序遍历
public class P102BinaryTreeLevelOrderTraversal{
    public static void main(String[] args) {
        Solution solution = new P102BinaryTreeLevelOrderTraversal().new Solution();
        // TO TEST
    }
    //leetcode submit region begin(Prohibit modification and deletion)
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    //BFS
    class Solution {
        public List<List<Integer>> levelOrder(TreeNode root) {
            List<List<Integer>> res = new ArrayList<>();
            Queue<TreeNode> q = new LinkedList<>();
            q.offer(root);
            while (!q.isEmpty()){
                int size = q.size();
                List<Integer> level = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    TreeNode cur = q.peek();
                    q.poll();
                    if (cur == null){
                        continue;
                    }
                    level.add(cur.val);
                    q.offer(cur.left);
                    q.offer(cur.right);
                }
                if (!level.isEmpty()){
                    res.add(level);
                }
            }
            return res;
        }
    }

    //DFS
//    class Solution {
//        public List<List<Integer>> levelOrder(TreeNode root) {
//            List<List<Integer>> res = new ArrayList<>();
//            if (root != null){
//                dfs(res, root, 0);
//            }
//            return res;
//        }
//
//        private void dfs(List<List<Integer>> res, TreeNode root, int level) {
//            if (res.size() - 1 < level){
//                res.add(new ArrayList<Integer>());
//            }
//            res.get(level).add(root.val);
//            if (root.left != null){
//                dfs(res, root.left, level + 1);
//            }
//            if (root.right != null){
//                dfs(res, root.right, level + 1);
//            }
//        }
//    }
//leetcode submit region end(Prohibit modification and deletion)

}