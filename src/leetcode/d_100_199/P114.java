package leetcode.d_100_199;

public class P114 {

    public void flatten(TreeNode root) {
        if (root == null) {
            return;
        }
        TreeNode right = root.right;
        TreeNode left = root.left;
        flatten(right);
        flatten(left);
        root.left = null;
        root.right = left;
        TreeNode node = root;
        while(node.right != null) {
            node = node.right;
        }
        node.right = right;
    }
}
