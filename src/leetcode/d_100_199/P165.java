package leetcode.d_100_199;

public class P165 {
    public static void main(String[] args) {
        P165 test = new P165();
        System.out.println(test.compareVersion("1.2", "1.10")); // -1
        System.out.println(test.compareVersion("1.01", "1.001")); // 0
        System.out.println(test.compareVersion("1.0", "1.0.0.0")); // 0
    }

    public int compareVersion(String version1, String version2) {
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        for (int i=0; i<v1.length || i<v2.length; i++) {
            int v1Value = 0;
            int v2Value = 0;
            if (i < v1.length) {
                v1Value = Integer.parseInt(v1[i]);
            }
            if (i < v2.length) {
                v2Value = Integer.parseInt(v2[i]);
            }
            if (v1Value > v2Value) {
                return 1;
            }
            if (v1Value < v2Value) {
                return -1;
            }
        }
        return 0;
    }
}
