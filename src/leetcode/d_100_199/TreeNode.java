package leetcode.d_100_199;

/**
 * @program: leetcode
 * @description TODO
 * @author: za-zhouyunxing
 * @create: 2020-05-26 12:54
 **/
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
    TreeNode(int x, TreeNode l, TreeNode r) {
        val = x;
        left = l;
        right = r;
    }
}
