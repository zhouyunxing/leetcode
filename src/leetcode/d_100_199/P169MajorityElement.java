//给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。 
//
// 你可以假设数组是非空的，并且给定的数组总是存在多数元素。 
//
// 
//
// 示例 1: 
//
// 输入: [3,2,3]
//输出: 3 
//
// 示例 2: 
//
// 输入: [2,2,1,1,1,2,2]
//输出: 2
// 
// Related Topics 位运算 数组 分治算法


package leetcode.d_100_199;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//Java：多数元素
public class P169MajorityElement{
    public static void main(String[] args) {
        Solution solution = new P169MajorityElement().new Solution();
        // TO TEST
    }
    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int majorityElement(int[] nums) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int num : nums) {
                map.putIfAbsent(num, 0);
                map.put(num, map.get(num)+1);
            }

            int n = nums.length;
            for (int key : map.keySet()) {
                if (map.get(key) > n/2) {
                    return key;
                }
            }
            return -1;
        }

        public int majorityElement2(int[] nums) {
            int len = nums.length;
            Arrays.sort(nums);
            return nums[len/2];
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}