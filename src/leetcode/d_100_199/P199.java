package leetcode.d_100_199;

import java.util.*;

public class P199 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode leftTree = new TreeNode(2);
        leftTree.right = new TreeNode(5);
        TreeNode rightTree = new TreeNode(3);
        rightTree.right = new TreeNode(4);
        root.left = leftTree;
        root.right = rightTree;
        P199.Solution solution = new P199().new Solution();
        System.out.println(solution.rightSideView(root));
    }

    class Solution {
        private final List<Integer> res = new ArrayList<>();

        public List<Integer> rightSideView(TreeNode root) {
            dfs(root, 0);
            return res;
        }

        public void dfs(TreeNode root, int depth) {
            if (root == null) {
                return;
            }
            if (depth == res.size()) {
                res.add(root.val);
            }
            dfs(root.right, depth+1);
            dfs(root.left, depth+1);
        }
    }
}
