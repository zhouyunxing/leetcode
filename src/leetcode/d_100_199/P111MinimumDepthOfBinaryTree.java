//给定一个二叉树，找出其最小深度。 
//
// 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。 
//
// 说明: 叶子节点是指没有子节点的节点。 
//
// 示例: 
//
// 给定二叉树 [3,9,20,null,null,15,7], 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 返回它的最小深度 2. 
// Related Topics 树 深度优先搜索 广度优先搜索


package leetcode.d_100_199;

import java.util.LinkedList;
import java.util.Queue;

//Java：二叉树的最小深度
public class P111MinimumDepthOfBinaryTree{
    public static void main(String[] args) {
        Solution solution = new P111MinimumDepthOfBinaryTree().new Solution();
        // TO TEST
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    class Solution {
        // 递归法
        public int minDepth(TreeNode root) {
            if (root == null) {
                return 0;
            }
            int minDepth = 0;
            if (root.left == null) {
                minDepth = minDepth(root.right);
            }else if (root.right == null) {
                minDepth = minDepth(root.left);
            }else {
                minDepth = Math.min(minDepth(root.left), minDepth(root.right));
            }
            return 1 + minDepth;
        }

        // bfs
        public int minDepth2(TreeNode root) {
            if (root == null){
                return 0;
            }
            Queue<TreeNode> queue = new LinkedList<>();
            int minDepth = 0;
            queue.offer(root);

            while (!queue.isEmpty()){
                minDepth++;

                int size = queue.size();
                for (int i = 0; i < size; i++) {
                    TreeNode node = queue.poll();
                    if (node == null){
                        continue;
                    }

                    if (node.left == null && node.right == null){
                        return minDepth;
                    }
                    queue.offer(node.left);
                    queue.offer(node.right);
                }
            }

            return minDepth;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}