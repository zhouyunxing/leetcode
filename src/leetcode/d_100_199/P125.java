package leetcode.d_100_199;

public class P125 {
    public static void main(String[] args) {
        P125 test = new P125();
        System.out.println(test.isPalindrome("A man, a plan, a canal: Panama")); // true
        System.out.println(test.isPalindrome("race a car")); // false
        System.out.println(test.isPalindrome("9,8")); // false
    }

    public boolean isPalindrome(String s) {
        int len = s.length();
        int i = 0;
        int j = len-1;
        while (i <= j) {
            while(i <= j && !isChar(s.charAt(i))) {
                i++;
            }
            while(i <= j && !isChar(s.charAt(j))) {
                j--;
            }
            if (i > j) {
                break;
            }
            char ch1 = Character.toUpperCase(s.charAt(i));
            char ch2 = Character.toUpperCase(s.charAt(j));
            if (!(ch1==ch2)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    public boolean isChar(char ch) {
        if (ch >= 'a' && ch < 'z') {
            return true;
        }
        if (ch >= 'A' && ch < 'Z') {
            return true;
        }
        if (ch >= '0' && ch <= '9') {
            return true;
        }
        return false;
    }
}
